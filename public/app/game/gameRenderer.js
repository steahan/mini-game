 var Renderer = (function() {
    
    function render($scope) {
      console.log('rendering');
      drawLevel($scope);
    }

    function drawLevel($scope) {
    	$scope.game.level[0].bombs;
    	for (var k = 0; k < $scope.game.level[0].bombs.length; k++) {
    		drawImage('/img/Bomb.png');
    	}
    }

    return {
      render : render,
      drawLevel : drawLevel
    }
 })();