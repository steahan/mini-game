angular.module('game')

  .controller('gameCtrl', [
      '$scope', 
      '$location', 
      'Updater', 
      'Renderer', 
      'Particles', 
    function(
      $scope, 
      $location, 
      Updater,    
      Renderer,
      Particles
  ) {


    $scope.title = 'Choose a game type';

    $scope.lastTimeStamp = performance.now();
    
    // CANVAS ELEMENTS
    $scope.canvas = document.getElementById('canvas');
    $scope.context = $scope.canvas.getContext('2d');


    $scope.game = {
      currentLevel : 0,
      playerScore : 0,
      bombs : [],
      level : [
        one = {
          bombs : [0, 1, 2, 3, 4, 5]
        },
        two = {
          bombs : [new Image(), new Image(), new Image(), 
                    new Image(), new Image(), new Image(),
                    new Image(), new Image(), new Image()
                    ]
        },
        three = {
          bombs : [new Image(), new Image(), new Image(),
                    new Image(), new Image(), new Image(),
                    new Image(), new Image(), new Image(), 
                    new Image(), new Image(), new Image()
                    ]
        },
        four = {
          bombs : [new Image(), new Image(), new Image(),
                    new Image(), new Image(), new Image(),
                    new Image(), new Image(), new Image(),
                    new Image(), new Image(), new Image(), 
                    new Image(), new Image(), new Image()
                    ]
        },
        five = {
          bombs : [new Image(), new Image(), new Image(),
                    new Image(), new Image(), new Image(),
                    new Image(), new Image(), new Image(),
                    new Image(), new Image(), new Image(),
                    new Image(), new Image(), new Image(), 
                    new Image(), new Image(), new Image()
                    ]
        },
      ]
    };

    /*******************************
            Image Paths
    ********************************/  
    for (var i = 0; i < 6; i++) {
      $scope.game.bombs.push(new Image());
      $scope.game.bombs[i].src = '/img/Bomb.png';
    }

    gameLoop = function(time) {
      elapsedTime = (time - $scope.lastTimeStamp) /1000;
      $scope.lastTimeStamp = time;

      Updater.update($scope, elapsedTime);
      Renderer.render($scope);
      
      if ($location.url() === '/game')
        requestAnimationFrame(gameLoop);
    }
    
    // requestAnimationFrame(gameLoop);

  }]);
