var Updater = (function() {

  function updateLevel ($scope, elapsedTime) {
    var roundOver = true;
    if (($scope.interval += elapsedTime) > $scope.countDownBuffer) {
      $scope.bombs.forEach(function (bomb) {
        if (!bomb.diffused && !bomb.exploded && bomb.countDown != 0 ) {
          bomb.countDown--;
          roundOver = false
        }
        if (bomb.countDown === 0 && !bomb.exploded) {
        bomb.exploded = true;
        $scope.score -= 5;
        $scope.$apply();
        for(var i=0; i < 10; i ++) {
          Particles.create($scope.fire, {x: bomb.x + 25, y: bomb.y + 25}, $scope.particles);
          Particles.create($scope.smoke, {x: bomb.x + 25, y: bomb.y + 25}, $scope.particles);
        }
      }
      });
      $scope.interval = 0;
      if (roundOver) {
        $scope.roundOver = true;
        $scope.bombs.forEach(function (bomb) {
          if (bomb.exploded)
            $scope.gameOver = true;
        });
      }
    }
  }

   function diffuseBombs ($scope) {
    var clickArrayLength = $scope.clicks.length;
    for (var i = 0; i < clickArrayLength; i++){
      var clickPosition = $scope.clicks.pop();
      $scope.bombs.forEach(function(bomb) { 
        if ((clickPosition.x >= bomb.x && clickPosition.x <= bomb.x + 50) && (clickPosition.y >= bomb.y && clickPosition.y <= bomb.y + 50)) {
          if (!bomb.exploded)
            bomb.diffused = true;
            $scope.score += bomb.countDown;
            $scope.$apply();
        }
      })
    }
    $scope.clicked = false;
  }

  function update ($scope, elapsedTime) {
    $scope.totalTime += elapsedTime;
    if ($scope.mouseClicked) {
      diffuseBombs($scope);
    }
    updateLevel($scope, elapsedTime);
    Particles.update($scope.particles, elapsedTime);
    $scope.$apply();
  }

    function updateCountdown($scope, elapsedTime) {
    if(($scope.interval += elapsedTime) > 1000) {
      $scope.countDown--;
      $scope.interval = 0;
    }
  }

  return {
    updateLevel : updateLevel,
    update : update,
    diffuseBombs : diffuseBombs,
    updateCountdown : updateCountdown
  };

})();