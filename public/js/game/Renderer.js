var Renderer = (function() {

  var NUMBERSIZE = 50,
      EXPLOSIONSIZE = 75;

  function drawImage(context, spec) {
    context.save();
    
    context.translate(spec.center.x, spec.center.y);
    context.rotate(spec.rotation);
    context.translate(-spec.center.x, -spec.center.y);
    
    context.drawImage(
      spec.image, 
      spec.center.x - spec.size/2, 
      spec.center.y - spec.size/2,
      spec.size, spec.size);
    
    context.restore();
  }

  function drawBombs($scope) {
    $scope.bombs.forEach(function (bomb, index) {
      $scope.context.drawImage($scope.game.bomb.image, bomb.x-15, bomb.y-14, 60, 60);
      $scope.context.drawImage($scope.numbers[bomb.countDown], bomb.x, bomb.y, 45, 45);
      if (bomb.exploded) {
        $scope.context.drawImage($scope.game.explosion.image, bomb.x-5, bomb.y-10, 60, 60);
      }
      if (bomb.diffused) {
        $scope.context.drawImage($scope.game.checkmark.image, bomb.x - 5, bomb.y - 5, 60, 60);
      }
    });
  }

  function drawCountdown($scope) {
    $scope.canvas.width = $scope.canvas.width;
    $scope.context.fillStyle = "#000";
    $scope.context.font = "100px Verdana, Geneva, sans-serif";
    
    drawBombs($scope);
    
    if ($scope.countDown <= 0) {
      $scope.context.fillText("Go!", 75, 200);
    }
    else {
      $scope.context.fillText($scope.countDown, 120, 200);
    }
  }


  function drawNumbers(context, numberArray, $scope) {
    var x = 271;
    var y = 81;
    var explosionX = 260;
    var explosionY = 65;
    var rowCount = 3;
    for (var k = 0; k < numberArray.length; k++) {
      if ($scope.game.level[$scope.game.currentLevel].values[k] <= 0) {
        context.drawImage($scope.game.explosion.image, explosionX, explosionY, EXPLOSIONSIZE, EXPLOSIONSIZE); //explosion image
        if ((k+1) % rowCount === 0) {
          y += 75;
          x = 171;
          explosionY += 75;
          explosionX = 160;
        }
        explosionX += 100;
        x += 100;
      }
      else {
        context.drawImage($scope.numbers[numberArray[k]], x, y, NUMBERSIZE, NUMBERSIZE); //number image
        if ((k+1) % rowCount === 0) {
          y += 75;
          x = 171;
          explosionY += 75;
          explosionX = 160;
        }
        x += 100;
        explosionX += 100;
      }
    }
  }

  function render($scope) {
    $scope.canvas.width = $scope.canvas.width; //Clear the canvas
    drawBombs($scope);
    Particles.render($scope.particles, $scope.context, drawImage);
  }

  return {
    render : render,
    drawBombs : drawBombs,
    drawImage : drawImage,
    drawNumbers : drawNumbers,
    drawCountdown : drawCountdown
  };

})();