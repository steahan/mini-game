angular.module('game')

.controller('gameCtrl', ['$scope', '$http', function($scope, $http) {
  
  $scope.initializeGame = function() {
    $scope.lastTimeStamp = performance.now();
    $scope.level = 1;
    $scope.countDown = 3;
    $scope.interval = 0;
    $scope.totalTime = 0;
    $scope.countDownBuffer = 3;
    $scope.score = 0;
    $scope.totalScore = 0;
    $scope.allScores = [];
    $scope.clicks = [];
    $scope.particles = [];
    $scope.gameOver = false;
    $scope.roundOver = false;

    fillBombs();
    requestAnimationFrame(countingDown);
  }

  initRound = function() {
    $scope.level++;
    $scope.roundOver = false;
    fillBombs();
    $scope.score = 0;
    $scope.countDown = 3;
  }

  $scope.numbers = [new Image(), new Image(), new Image(),new Image(),new Image(),
                    new Image(), new Image(),new Image(),new Image(),new Image()
                    ];

  // BOMB IMAGES IN ARRAY:
  $scope.numbers[0].src = '/img/glass_numbers_0.png';
  $scope.numbers[1].src = '/img/glass_numbers_1.png';
  $scope.numbers[2].src = '/img/glass_numbers_2.png';
  $scope.numbers[3].src = '/img/glass_numbers_3.png';
  $scope.numbers[4].src = '/img/glass_numbers_4.png';
  $scope.numbers[5].src = '/img/glass_numbers_5.png';
  $scope.numbers[6].src = '/img/glass_numbers_6.png';
  $scope.numbers[7].src = '/img/glass_numbers_7.png';
  $scope.numbers[8].src = '/img/glass_numbers_8.png';
  $scope.numbers[9].src = '/img/glass_numbers_9.png';

  // THE GAME OBJECT
  $scope.game = {
    currentLevel : 0,
    playerScore : 0,
    time : 0,
    level : [
      one = {
        bombs : 6,
        notCompleted : true,
        values : [3, 3, 2,
                  2, 1, 1
                ]
        },
      two = {
        bombs : 9,
        notCompleted : true,
        values : [3, 3, 2,
                  2, 1, 1,
                  4, 3, 2
                ]
        },
      three = {
        bombs : 12,
        notCompleted : true,
        values : [3, 3, 2,
                  2, 1, 1,
                  4, 3, 2,
                  5, 4, 3
                ]
        },
      four = {
        bombs : 15,
        notCompleted : true,
        values : [3, 3, 2,
                  2, 1, 1,
                  4, 3, 2,
                  5, 4, 3,
                  6, 5, 4
                ]
        },
      five = {
        bombs : 18,
        notCompleted : true,
        values : [3, 3, 2,
                  2, 1, 1,
                  4, 3, 2,
                  5, 4, 3,
                  6, 5, 4,
                  7, 6, 5
                ]
        }
    ],
    bomb : {
      image: new Image()
    },
    explosion : {
      image: new Image()
    },
    checkmark : {
      image: new Image()
    }
  };

    // IMAGE PATHS
  $scope.game.bomb.image.src = "/img/Bomb.png";
  $scope.game.explosion.image.src = "/img/Explosion.png";
  $scope.game.checkmark.image.src = '/img/checkmark.png';
  var fire = new Image();
  fire.src = '/img/fire.png';
  $scope.fire = {
    image: fire,
    speed: {mean: 100, stdev: 50},
    lifetime: {mean: 5, stdev: 2}
  };
  var smoke = new Image();
  smoke.src = '/img/smoke.png';
  $scope.smoke = {
    image: smoke,
    speed: {mean: 100, stdev: 50},
    lifetime: {mean: 5, stdev: 2}
  };

  bombValues = [3, 3, 2, 2, 1, 1, 4, 3, 2, 5, 4, 3, 6, 5, 4, 7, 6, 5]

  // http://bost.ocks.org/mike/shuffle/
    function Shuffle(array) {
    var currentIndex = array.length,
      temporaryValue,
      randomIndex;
    while (0 !== currentIndex) { // while there are elements in the array
      randomIndex = ~~(Math.random() * currentIndex); // pick an element
      currentIndex -= 1;
      temporaryValue = array[currentIndex]; // and swap it with the current element
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }  
    return array;
  }

  fillBombs = function() {
    $scope.bombs = [];
    for (var i = 0; i <= ($scope.level+1) * 3 - 1; i++) {
      $scope.bombs.push({
        countDown: bombValues[i], // gives each bomb a number from bombValues
        diffused: false, // adds a property of diffused for mouse clicks
        exploded: false
      });
    }
    Shuffle($scope.bombs);
    $scope.bombs.forEach(function(bomb, index) {
        bomb.x = (440 - (80 * (index % 3))),              // adds an x coordinate
        bomb.y = (100 + (70 * Math.floor(index / 3)))     // adds a y coordinate
    })
  }

    // CANVAS ELEMENTS
  $scope.canvas = document.getElementById('canvas');
  $scope.context = $scope.canvas.getContext('2d');

  mouseClicked = function(e) {
    $scope.mouseClicked = true;
    e.preventDefault();
    $scope.clicks.push({x: e.offsetX, y: e.offsetY})
  }

  // EVENTS
  $scope.canvas.onclick = mouseClicked;

  countingDown = function(time) {
    $scope.clicks = [];
    elapsedTime = (time - $scope.lastTimeStamp);
    $scope.lastTimeStamp = time;
    if ($scope.countDown === -1) {
      requestAnimationFrame(gameLoop);
    }
    else {
      Updater.updateCountdown($scope, elapsedTime);
      Renderer.drawCountdown($scope);
      requestAnimationFrame(countingDown);
    }
  }

  gameLoop = function(time) {
    elapsedTime = (time - $scope.lastTimeStamp) / 1000;
    $scope.lastTimeStamp = time;

    Updater.update($scope, elapsedTime);
    Renderer.render($scope);
    
    if(!$scope.gameOver && !$scope.roundOver) {
      requestAnimationFrame(gameLoop);
    }
    else if ($scope.roundOver) {
      $scope.allScores.push($scope.score);
      initRound();
      $scope.allScores.forEach(function(score) {
        $scope.totalScore += score;
      });
      $scope.$apply();
      if ($scope.level > 5) 
        $scope.gameOver = true;
      
      if($scope.gameOver) {
        // Store scores 
        var scoresObject = {scores: $scope.allScores, totalScore: $scope.totalScore, time: Math.floor($scope.totalTime/1000),level: $scope.level-1}
        var scores = JSON.parse(localStorage.getItem('scores'));
        if (scores === false)
          scores = [];
        scores.push(scoresObject);
        localStorage.setItem('scores', JSON.stringify(scores));
      }
      else 
        requestAnimationFrame(countingDown);
    }
  }

}]);
