angular.module('game')

.controller('scoreCtrl', ['$scope', '$http', function($scope, $http) {

	$scope.scores = JSON.parse(localStorage.getItem('scores'));
  $scope.title = 'High Scores';

}]);